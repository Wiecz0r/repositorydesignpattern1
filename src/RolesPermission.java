
public class RolesPermission {
	private String roleID;
	private String permissionID;
	public String getRoleID() {
		return roleID;
	}
	public void setRoleID(String roleID) {
		this.roleID = roleID;
	}
	public String getPermissionID() {
		return permissionID;
	}
	public void setPermissionID(String permissionID) {
		this.permissionID = permissionID;
	}
	
	
}