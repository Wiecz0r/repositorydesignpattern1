
public interface UnitOfWork {

	void saveChanges();
	void undo();
	void markAsNew(Entity entity, UnitOfWorkRepository repo);
	void markAsDeleted(Entity entity, UnitOfWorkRepository repo);
	void markAsChanged(Entity entity, UnitOfWorkRepository repo);
}

