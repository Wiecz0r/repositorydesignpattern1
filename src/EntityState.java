
public enum EntityState {
	New, Modified, Unchanged, Deleted, Unknown;
}